export const LEADERS = [
  {
    id: 0,
    name: "Anish Subedi",
    image: "/assets/images/alberto.png",
    designation: "Chief Executive Officer",
    abbr: "CEO",
    featured: false,
    description:
      "Our CEO is the world best entrepreneur. He believes in keeping the health of his customers as a first pirotity. Come first Serve first ! Taste it ! "
  },
  {
    id: 1,
    name: "Krishna Shapkota",
    image: "/assets/images/alberto.png",
    designation: "Chief Food Officer",
    abbr: "CFO",
    featured: false,
    description:
      "Our CFo is the American Return with the huge experience on the Food technology and Food serivices. He believe in serivng with the best experience"
  },
  {
    id: 2,
    name: "Birat Shrestha",
    image: "/assets/images/alberto.png",
    designation: "Chief Taste Officer",
    abbr: "CTO",
    featured: false,
    description:
      "One of the world best cheif. He has been awarded with multiple awards in food technology."
  },
  {
    id: 3,
    name: "Srijan shrestha",
    image: "/assets/images/alberto.png",
    designation: "Executive Chef",
    abbr: "EC",
    featured: true,
    description:
      "One the topper in USA in his field. He believe in giving the best to get the best"
  }
];
