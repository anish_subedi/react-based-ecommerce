export const COMMENTS = [
  {
    id: 0,
    dishId: 0,
    rating: 5,
    comment:
      "It was total yummy. The best MOMO that i have ever eaten till now",
    author: "Rabin Sharma",
    date: "2075-05-09"
  },
  {
    id: 1,
    dishId: 0,
    rating: 4,
    comment: "It was the yummiest that i have ever eaten",
    author: "kabin shrestha",
    date: "2075-01-02"
  },
  {
    id: 2,
    dishId: 0,
    rating: 3,
    comment: "Eat it, just eat it!",
    author: "My favourate items",
    date: "2075-11-12"
  },
  {
    id: 3,
    dishId: 0,
    rating: 4,
    comment: "My best item",
    author: "Mr. Ram",
    date: "2075-11-18"
  },
  {
    id: 4,
    dishId: 0,
    rating: 2,
    comment: "Dami Mitho ra babal",
    author: "Mr.Anish",
    date: "2075-01-01"
  },
  {
    id: 5,
    dishId: 1,
    rating: 5,
    comment: "Go healthy",
    author: "Birat shrestha",
    date: "2075-05-05"
  },
  {
    id: 6,
    dishId: 1,
    rating: 4,
    comment: "My new fav item from today",
    author: "Bibash Shrestha",
    date: "2076-01-02"
  },
  {
    id: 7,
    dishId: 1,
    rating: 3,
    comment: "Eat it, just eat it!",
    author: "Mr.Srijan",
    date: "2075-09-06"
  },
  {
    id: 8,
    dishId: 1,
    rating: 4,
    comment: "My best item",
    author: "Mr. Ram",
    date: "2075-11-18"
  },
  {
    id: 9,
    dishId: 1,
    rating: 2,
    comment: "Dami Mitho ra babal",
    author: "Mr.Anish",
    date: "2075-01-01"
  },
  {
    id: 10,
    dishId: 2,
    rating: 5,
    comment: "Go healthy",
    author: "Birat shrestha",
    date: "2075-05-05"
  },
  {
    id: 11,
    dishId: 2,
    rating: 4,
    comment: "My new fav item from today",
    author: "Bibash Shrestha",
    date: "2076-01-02"
  },
  {
    id: 12,
    dishId: 2,
    rating: 3,
    comment: "Eat it, just eat it!",
    author: "Mr.Srijan",
    date: "2075-09-06"
  },
  {
    id: 13,
    dishId: 2,
    rating: 4,
    comment: "My best item",
    author: "Mr. Ram",
    date: "2075-11-18"
  },
  {
    id: 14,
    dishId: 2,
    rating: 2,
    comment: "Dami Mitho ra babal",
    author: "Mr.Anish",
    date: "2075-01-01"
  },
  {
    id: 15,
    dishId: 3,
    rating: 5,
    comment: "Go healthy",
    author: "Birat shrestha",
    date: "2075-08-09"
  },
  {
    id: 16,
    dishId: 3,
    rating: 4,
    comment: "My new fav item from today",
    author: "Bibash Shrestha",
    date: "2076-01-02"
  },
  {
    id: 17,
    dishId: 3,
    rating: 3,
    comment: "Eat it, just eat it!",
    author: "Mr.Srijan",
    date: "2075-09-06"
  },
  {
    id: 18,
    dishId: 3,
    rating: 4,
    comment: "My best item",
    author: "Mr. Ram",
    date: "2075-11-18"
  },
  {
    id: 19,
    dishId: 3,
    rating: 2,
    comment: "Dami Mitho ra babal",
    author: "Mr.Anish",
    date: "2075-01-01"
  }
];
