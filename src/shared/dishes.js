export const DISHES = [
  {
    id: 0,
    name: "MOMO",
    image: "/assets/images/MOMO.png",
    category: "mains",
    label: "Hot",
    price: "4.99",
    featured: true,
    description: "Complete Nepali Momo khanus afnai para mah"
  },
  {
    id: 1,
    name: "Nepali Dhal Bhaat",
    image: "/assets/images/Dalbhat.png",
    category: "appetizer",
    label: "",
    price: "1.99",
    featured: false,
    description:
      "A total traditional style Dhal Bhaat with a complete 8 items. Enjoy !"
  },
  {
    id: 2,
    name: "Yomari",
    image: "/assets/images/yomari.png",
    category: "appetizer",
    label: "New",
    price: "1.99",
    featured: false,
    description:
      "A perfect Newari dish for a Perfect Day. All avaliable in 2 different ways. Enjoy !"
  },
  {
    id: 3,
    name: "Buffs",
    image: "/assets/images/buffs.png",
    category: "dessert",
    label: "",
    price: "2.99",
    featured: false,
    description:
      "We focus on total health. A complete healthy packet of Buff items. Enjoy !"
  }
];
