import React, { Component } from "react";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  Breadcrumb,
  BreadcrumbItem
} from "reactstrap";
import { Link } from "react-router-dom";
import Menu from "./MenuComponent";

function RenderDish({ item }) {
  console.log(item);
  if (item != null)
    return (
      <div className="container">
        <div className="row">
          <Breadcrumb>
            <BreadcrumbItem>
              <Link to="/menu">Menu</Link>
            </BreadcrumbItem>
            <BreadcrumbItem active>{item.name} </BreadcrumbItem>
          </Breadcrumb>
          <div className="col-12" />
          <h3>{item.name}</h3>
          <hr />
        </div>

        <div className="row">
          <div className="col-12 col-md-5 m-1">
            <Card className="col-12 col-md-12">
              <CardImg top src={item.image} alt={item.name} />
              <CardBody>
                <CardTitle>{item.name}</CardTitle>
                <CardText>{item.description}</CardText>
                <CardText>{item.comment}</CardText>
              </CardBody>
            </Card>
          </div>
        </div>
      </div>
    );
  else return <div> No items here</div>;
}

function RenderComments({ comments }) {
  const items1 = comments.map(dish => {
    console.log(dish);
    return (
      <div>
        <div key={dish.id}>
          <ul>
            <li> {dish.comment}</li>
            <li>
              --{dish.author} , {dish.date}
            </li>
          </ul>
        </div>
      </div>
    );
  });

  if (comments != null) {
    return (
      <div>
        <h4>Comments</h4>
        {items1}
      </div>
    );
  } else {
    return <div />;
  }
}
const DishDetail = props => {
  console.log("Initial props rendered here");
  console.log(props.dish);
  return (
    <div className="container">
      <div className="row">
        <Breadcrumb>
          <BreadcrumbItem>
            <Link to="/menu">Menu</Link>
          </BreadcrumbItem>
          <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
        </Breadcrumb>
        <div className="col-12">
          <h3>{props.dish.name}</h3>
          <hr />
        </div>
      </div>
      <div className="row">
        <div className="col-6 col-md-6 ">
          <RenderDish item={props.dish} />
        </div>
        <div className="col-6 col-md-5">
          <RenderComments comments={props.comments} />
        </div>
      </div>
    </div>
  );
};

export default DishDetail;
