import React from "react";
import {
  Breadcrumb,
  BreadcrumbItem,
  Card,
  CardBody,
  CardHeader,
  Media
} from "reactstrap";
import { Link } from "react-router-dom";

function RenderLeader({ leaders }) {
  let imgStyle = {
    maxHeight: "100px",
    maxWidth: "100px"
  };

  return (
    <div>
      <Media className="mt-1">
        <Media left middle href="#">
          <Media
            style={imgStyle}
            object
            src={leaders.image}
            alt="Image leaders"
            className="col xs=3"
          />
        </Media>
        <Media body className="col xs=auto">
          <Media heading>{leaders.name}</Media>
          {leaders.designation}
          <br />
          {leaders.description}

          <br />
        </Media>
      </Media>
    </div>
  );
}

function About(props) {
  console.log(props);
  console.log(props.leaders);
  const leaders = props.leaders.map(leader => {
    return (
      <div>
        <RenderLeader leaders={leader} />
      </div>
    );
  });

  return (
    <div className="container">
      <div className="row">
        <Breadcrumb>
          <BreadcrumbItem>
            <Link to="/home">Home</Link>
          </BreadcrumbItem>
          <BreadcrumbItem active>About Us</BreadcrumbItem>
        </Breadcrumb>
        <div className="col-12">
          <h3>About Us</h3>
          <hr />
        </div>
      </div>
      <div className="row row-content">
        <div className="col-12 col-md-6">
          <h2>Our History</h2>
          <p>
            Started in back 2074 we had the best team. A best team from the CEO
            to the best chef of the Market. We believe in giving the best to
            take the best. One of the best food service in the Nepal. Trust US !
            Enjoy US ! We keep your health as our first priority. Never take
            your health at risk.
          </p>
          <p>
            The restaurant traces its humble beginnings to{" "}
            <em>The Foodie Nepal</em>, a successful chain started by our CEO,
            Mr. Anish Subedi, that featured for the first time the world's best
            Food service.
          </p>
        </div>
        <div className="col-12 col-md-5">
          <Card>
            <CardHeader className="bg-primary text-white">
              Facts At a Glance
            </CardHeader>
            <CardBody>
              <dl className="row p-1">
                <dt className="col-6">Started</dt>
                <dd className="col-6">04-Fed-2074</dd>
                <dt className="col-6">Major Stake Holder</dt>
                <dd className="col-6"> Fine Foods Inc.</dd>
                <dt className="col-6">Last Year's Turnover</dt>
                <dd className="col-6">2 Million</dd>
                <dt className="col-6">Employees</dt>
                <dd className="col-6">40</dd>
              </dl>
            </CardBody>
          </Card>
        </div>
        <div className="col-12">
          <Card>
            <CardBody className="bg-faded">
              <blockquote className="blockquote">
                <p className="mb-0">
                  Get the full service for what you pay for.
                </p>
                <footer className="blockquote-footer">
                  Anish,
                  <cite title="Source Title">
                    Your Money, Your Health, Our Service !
                  </cite>
                </footer>
              </blockquote>
            </CardBody>
          </Card>
        </div>
      </div>
      <div className="row row-content">
        <div className="col-12 offset-1">
          <h2>Corporate Leadership</h2>
        </div>
        <div className="col-12 offset-1">
          <Media list>{leaders}</Media>
        </div>
      </div>
    </div>
  );
}

export default About;
